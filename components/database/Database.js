

export const COLORS = {
    white: '#ffffff',
    black: '#000000',
    green: '#00AC76',
    red: '#C04345',
    blue: '#4d28a6',
    skyBlue: '#54c9f0',
    backgroundLight: '#F0F0F3',
    backgroundMedium: '#B9B9B9',
    backgroundDark: '#777777',

}

export const Items = [
    {
      id: 1,
      category: 'sneakers',
      productName: 'Blue shoes',
      productPrice: 1799,
      description:
        'Used for sports | Light weight | 6 inches ',
      isOff: true,
      offPercentage: 10,
      productImage: require('../database/images/sneakers/1.jpg'),
      isAvailable: true,
      productImageList: [
        require('../database/images/sneakers/1.jpg'),
        require('../database/images/sneakers/1.jpg'),
        require('../database/images/sneakers/1.jpg'),
      ],
    },
    {
        id: 2,
        category: 'sneakers',
        productName: 'Red shoes',
        productPrice: 2799,
        description:
          'Used for sports | Light weight | 6 inches ',
        isOff: true,
        offPercentage: 10,
        productImage: require('../database/images/sneakers/2.jpg'),
        isAvailable: true,
        productImageList: [
          require('../database/images/sneakers/2.jpg'),
          require('../database/images/sneakers/2.jpg'),
          require('../database/images/sneakers/2.jpg'),
        ],
    },
    {
        id: 3,
        category: 'sneakers',
        productName: 'Navy blue shoes',
        productPrice: 999,
        description:
          'Used for sports | Light weight | 6 inches ',
        isOff: true,
        offPercentage: 10,
        productImage: require('../database/images/sneakers/3.jpg'),
        isAvailable: true,
        productImageList: [
          require('../database/images/sneakers/3.jpg'),
          require('../database/images/sneakers/3.jpg'),
          require('../database/images/sneakers/3.jpg'),
        ],
    },
    {
        id: 4,
        category: 'sneakers',
        productName: 'Yellow shoes',
        productPrice: 1999,
        description:
          'Used for sports | Light weight | 6 inches ',
        isOff: true,
        offPercentage: 10,
        productImage: require('../database/images/sneakers/4.jpg'),
        isAvailable: true,
        productImageList: [
          require('../database/images/sneakers/4.jpg'),
          require('../database/images/sneakers/4.jpg'),
          require('../database/images/sneakers/4.jpg'),
        ],
      },
      {
        id: 5,
        category: 'sneakers',
        productName: 'Pink girl shoes',
        productPrice: 2799,
        description:
          'Used for sports | Light weight | 6 inches ',
        isOff: true,
        offPercentage: 10,
        productImage: require('../database/images/sneakers/5.jpg'),
        isAvailable: true,
        productImageList: [
          require('../database/images/sneakers/5.jpg'),
          require('../database/images/sneakers/5.jpg'),
          require('../database/images/sneakers/5.jpg'),
        ],
      },
      
  ];